cluster_name           = "k1"
dc_name                = "cni"
datastore_name         = "datastore2"
datastore_cluster_name = " DatastoreCluster"
folder_name            = "k1"
network_name           = "epicshit-vlan69"
compute_cluster_name   = "cni-cluster"
control_plane_template = "u2004ci"
worker_template        = "u2004ci"
load_balancer_template = "u2004ci"
ssh_public_key_file    = "~/.ssh/k8s_rsa.pub"
ssh_username           = "ubuntu"
# initial count of workers
worker_count = 4
# initial count of control plane nodes
control_plane_count = 3
worker_memory       = 4096
# More variables can be overridden here, see variables.tf
