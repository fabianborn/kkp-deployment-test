# KKP on AutoPilot

This structure includes a fully automated setup of Kubermatic Kubernetes Platform
on top of vsphere with integration of Flux v2 for GitOps delivery and SOPS for secrets management.

## Used Components and Tools

 * Terraform - for automated resources provisioning in vsphere
 * KubeOne - for k8s master cluster preparation
 * KKP installer - for installing core KKP components on master cluster
 * Flux v2 - for managing k8s resources on master cluster with GitOps
 * SOPS, using Age backend - for safe storage of sensitive setup in GitHub
 * GitLab CI/CD - for the fully fledged delivery pipeline, 0 steps are done manually
   (except the `git init`, `git commit` and `git push`!)

## Preparation
### Create GitLab repository

Create new repository on GitLab [manually](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project).

Also prepare a GitLab API token which will be used for GitOps setup.

### Get your vsphere credentials
Terraform provider will need to set the environment variables to access the vSphere instance.

See there related KubeOne [documentation](https://docs.kubermatic.com/kubeone/master/guides/credentials/#environment-variables) and
pay attention to the vSphere specific [permissions](https://docs.kubermatic.com/kubeone/master/architecture/requirements/machine_controller/vsphere/vsphere/).

### Generate SSH keys

SSH public/private key-pair is used for accessing the master cluster nodes. You can generate these keys locally,
and you will need to set them inside the secret management below.

You can use following command to generate the keys:

```bash
ssh-keygen -t rsa -b 4096 -C "admin@kubermatic.com"
```

You will be prompted to provide a key location, e.g. `k8s_rsa`.
### Setup GitLab CI/CD Variables for the pipeline

Go to your GitLab project under "Settings" -> "CI/CD" -> "Variables" and setup following variables:
* `VSPHERE_SERVER` with value of your vCenter server name for vSphere API operations
* `VSPHERE_USER` with value of your username for vSphere API operations
* `VSPHERE_PASSWORD` with value of your password for vSphere API operations
* `SOPS_AGE_SECRET_KEY` with value of generated AGE secret key for SOPS (see secrets.md file)
* `GITLAB_TOKEN` with value of GitLab API token from above step
* `SSH_PRIVATE_KEY` with value of private SSH key (e.g. `k8s_rsa`), make sure to create variable with "Type" `Variable`
* `SSH_PUBLIC_KEY` with value of public SSH key (e.g. `k8s_rsa.pub`), make sure to create variable with "Type" `File`

### Push Content to your Git repository

```bash
git init
git checkout -b main
git add .
git commit -m "Initial setup for KKP on Autopilot"
git remote add origin git@gitlab.com:<GITLAB_OWNER>/<GITLAB_REPOSITORY>
git push -u origin main
```

### Validation
Check the steps of the GitLab CI/CD Pipeline after first merge to `main` branch and enjoy the full deployment of KKP at the end!

## High-level Pipeline Design

*tf-validate*
* runs validation of all Terraform modules

*tf-prepare*
* prepare Terraform backend for storing state

*tf-plan*
* prepare Terraform plan based on the stored state
* Terraform state is stored on AWS S3 bucket (created in previous step)

*tf-apply*
* applies the Terraform changes based on a plan (from previous stage)
* runs only on `main` branch
==> VMs, network and LB for k8s on AWS is prepared at this stage

*kubeone-apply*
* performs the cluster provisioning using the `kubeone` tool
* runs only on `main` branch!
==> k8s cluster is ready to use at this stage

*kkp-deploy*
* performs the Kubermatic Kubernetes Platform installation with installer
* runs only on `main` branch!
==> KKP platform with core components is prepared at this stage

*flux-bootstrap*
* initiate Flux v2 using `flux bootstrap gitlab` command
* runs only on `main` branch!
==> monitoring stack for KKP and other KKP resources (seed, preset, project) are delivered after Flux is set up on cluster,
Flux itself is also managed by the same GitHub repository

## Secrets management

Sensitive values are encrypted using the [SOPS](https://fluxcd.io/docs/guides/mozilla-sops/)
together with using the Age secret pair (generated from start.kubermatio.io).

Public AGE key for encryption is: `age14h358nwdrgualnhzwlvgt79v7cghqaf45fmajw6t7lu4jclj5ghsfzexez`.

## Operational tasks

See the various operational tasks in documentation:
 * [get access to Kubernetes cluster](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/access_to_cluster/)
 * [validate cluster and KKP readiness](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/validate_cluster_health/)
 * [work with secrets using SOPS](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/work_with_secrets/)
 * and [others](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/).
